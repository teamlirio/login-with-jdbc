package com.normz.login.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.normz.login.model.UserDetails;

@WebServlet("/validates")
public class UserValidateProcess extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
			response.setContentType("text/html");
			PrintWriter out = response.getWriter();
			String u = request.getParameter("user");
			String p = request.getParameter("pass");
			UserDetails ud = new UserDetails(u,p);
			
			ValidateUser vu = new ValidateUser();
		
			String username =  vu.userValidate(ud).toString();
			if(username.equals("Fail")) {
			
			out.println(username);
			out.println("<p>Please <a href='loginpage.jsp'>login</a> first</p>");
		
			}
			else {
			out.println("<h1>Hello, " + ud.getUsername() + "</h1>");
			HttpSession session = request.getSession();
			session.setAttribute("thisIsYourSession", ud.getUsername());
			out.println("Go to <a href='profile'>Profile</a>.");
			}
//			Cookie cookie = new Cookie("thisCookieIsMine",username);
//			response.addCookie(cookie);
			
			
	}

}
