package com.normz.login.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


@WebServlet("/profile")
public class UserProfile extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		
		HttpSession newSession = request.getSession(false);
		if(newSession != null) {
			String sessionName = (String) newSession.getAttribute("thisIsYourSession");
			out.println("<h1>Hello, " + sessionName + "</h1>");
			if(sessionName.equals("rachel")) {
				out.println("I LOVE YOU BEBEB!");
			}
			out.println("Click <a href='logout'>here</a> to logout.");
		}
		else {
			out.println("<p>Please <a href='loginpage.jsp'>login</a> first</p>");
		}
	}

}
