package com.normz.login.controller;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class UserJDBC {
	private static final String JDBC_URL = Messages.getString("UserJDBC.JDBC_URL"); //$NON-NLS-1$
	private static final String JDBC_USER = Messages.getString("UserJDBC.JDBC_USER"); //$NON-NLS-1$
	private static final String JDBC_PASS = Messages.getString("UserJDBC.JDBC_PASS"); //$NON-NLS-1$
	private Connection c;
	
	public Connection getConnection() {
		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			c = DriverManager.getConnection(JDBC_URL,JDBC_USER,JDBC_PASS);
			
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException sqle) {
			System.out.println(sqle.getMessage());
		}
		return c;
		
	}
	public static void main(String[] args) {
		UserJDBC u = new UserJDBC();
		System.out.println(u.getConnection());
	}
}
